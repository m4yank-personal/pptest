# PomeloPay Test - Mayank

Thank you for the opportunity for me to attempt this test. 

1) Backend
    
    - Resides under api folder, in env files as per the Node environment you can setup port, mongo_uri and database name. The current path of database needs whitelisting of IP.
    
    - Install node modules with npm install
        ```sh
        $ cd api
        $ npm install
        ```
        
    - In total 4 commands that can be used: start, server, build and test-dev
        -- start: will execute all trnaspile js (does not build it from ts).
        
            ```sh
            $ npm build 
            $ npm start
            ```
        -- server: runs ts files with nodemon
             ```sh
            $ npm run server
            ```
            
        -- build: will transpile ts to js after which you can run js
            ```sh
            $ npm run build
            ```
        -- test-dev: will run the unit test module for testing API's on test database
            ```sh
            $ npm run test-dev
            ```
            
    -  To understand fields and API's postman collection file is in root directory 'PomeloPay.postman_collection.json'
    



2) Frontend


    - Resides under pomelopay folder, no config setup in this one, just one .env file to change port.
    
    - Basic tsconfig.json
    
    - Endpoint for API is under src/transaction/data/Api.ts which is set to localhost:3000 as that is the port and path when ran on local for backend.
    
    - Data is being fetched from backend and hence having some transactions in database makes it visible on frontend.
    
    - To install node modules: 
    
        ```sh
        $ cd pomelopay
        $ npm install
        ```
        
    - To start the script and view on 4000 port (as set in .env file)
    
        ```sh
        $ npm run start
        ```
        
    - To build a production version of script
    
        ```sh
        $ npm run build
        ```
        
    - Using redux, saga, axios, moment, react-data-table-component for this view.
    
    - By default account number passed is '1231231223123', you can change the same in /src/transaction/sagas/Transaction.ts line number 15
    
    - Sortable columns are: ID, Date, Credit/Debit, Amount & Balance

