import {expect} from 'chai';
import {agent as request} from 'supertest';
import mongoose from "mongoose";
import Server from '../src/app';

process.env.NODE_ENV = "test";

describe("App Test", ()=>{
    it('should always pass', function(){
        expect(true).to.equal(true);
    });

    before(function(done) {
        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', function () {
            console.log('We are connected to test database!');
            return done();
        });
    });
    describe("Transaction Creation Use Cases", ()=>{
        it('Should post /transactions all fields correctly, credit the value', async function(){
            const res = await request(Server.server).post('/transactions').send({
                'accountNumber':'1231231223123',
                'currency':'GBP',
                'typeofTransaction':'credit',
                'amount':'100',
                'transactionDescription':'Add 100 Pounds',
                'totalBalance':'100'});
            expect(res.status).to.equal(201);
            expect(res.body.id).not.to.be.empty;
        });
        it('Should post /transactions all fields correctly, debit the value', async function(){
            const res = await request(Server.server).post('/transactions').send({
                'accountNumber':'1231231223123',
                'currency':'GBP',
                'typeofTransaction':'debit',
                'amount':'100',
                'transactionDescription':'Minus 100 Pounds',
                'totalBalance':'100'});
            expect(res.status).to.equal(201);
            expect(res.body.id).not.to.be.empty;
        });
        it('Should post /transactions Where currency is different then previous transactions', async function(){
            const res = await request(Server.server).post('/transactions').send({
                'accountNumber':'1231231223123',
                'currency':'SGD',
                'typeofTransaction':'credit',
                'amount':'100',
                'transactionDescription':'Add 1000 Pounds',
                'totalBalance':'100'});
            expect(res.status).to.equal(422);
            expect(res.body.message).not.to.be.empty;
            expect(res.body.message).equals('Please enter a valid currency that is being used by this account, we support only SGD or GBP for now.');
        });
        it('Should post /transactions Where currency is missing', async function(){
            const res = await request(Server.server).post('/transactions').send({
                'accountNumber':'1231231223123',
                'typeofTransaction':'credit',
                'amount':'100',
                'transactionDescription':'Add 1000 Pounds',
                'totalBalance':'100'});
            expect(res.status).to.equal(422);
            expect(res.body.message).not.to.be.empty;
            expect(res.body.message).equals('Please provide a currency for this transaction');
        });
        it('Should post /transactions Where account number is missing or wrong', async function(){
            const res = await request(Server.server).post('/transactions').send({
                'accountNumber':'12312312233',
                'typeofTransaction':'credit',
                'currency':'GBP',
                'amount':'100',
                'transactionDescription':'Add 1000 Pounds',
                'totalBalance':'100'});
            expect(res.status).to.equal(422);
            expect(res.body.message).not.to.be.empty;
            expect(res.body.message).equals('Please provide a valid account number for this transaction, it should be 13 digits long and should be numeric.');
        });
        
        it('Should get /transactions with account number', async function(){
            const res = await request(Server.server).get('/transactions/?accountNumber=1231231223123').send();
            expect(res.status).to.equal(200);
            expect(res.body).to.be.an("array");
            expect(res.body[0].accountNumber).equals(1231231223123);
        });

        it('Should get /transactions without account number', async function(){
            const res = await request(Server.server).get('/transactions/').send();
            expect(res.status).to.equal(422);
            expect(res.body.message).not.to.be.empty;
            expect(res.body.message).equals('Please provide a valid account number for which data needs to be fetched, it should be 13 digits long and should be numeric.');
        });
    });

});