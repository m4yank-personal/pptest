import * as mongoose from 'mongoose';
import {TransactionsSchema} from '../models/transactionsModels';
import {Request,Response, request} from 'express';

const Transaction = mongoose.model('Transaction', TransactionsSchema);

export class TransactionController{

    accountNumber:number;
    currency:string;
    totalAmount:number;

    constructor(){
        this.accountNumber=1231231231233;
        this.currency = 'SGD';
        this.totalAmount = 0;
    }
    
    //Fetches account's last entry for this particular account number to add new entry and check conditions are being satisfied.
    async getDataForAccount(accountNumber:number):Promise<void>
    {
        
        await Transaction.findOne({'accountNumber':accountNumber},"accountNumber currency totalBalance",{sort:{'created_date':'desc'}},(err:any,data:any)=>{
            if(err)
            {
                console.log(`Error: ${err}`);
            }
            else if (data)
            {
                this.accountNumber = data.accountNumber;
                this.currency = data.currency;
                this.totalAmount = parseFloat(data.totalBalance);
            }
        });
    }

    //Change in total amount as per type of transaction
    calculateTotalAmount(typeofTransaction:string, amount:number, res:Response):boolean
    {
        
        if(typeofTransaction == 'credit')
        {
            this.totalAmount =  this.totalAmount + amount;
            return true;
        }
        else
        {
            if(this.totalAmount - amount > 0)
            {
                this.totalAmount = this.totalAmount - amount;
                return true;
                
            }
            else
            {
                res.status(422).json({
                    'message':'Insufficient amount, kindly add money to your account first'
                });
                return false;
            }
        }
    }

    //Check if the account and the new transaction for that account are using same currency (ideally need to have different accounts or convert currency amount)
    checkCurrency(currency: string):boolean
    {
        if(this.currency == currency && this.totalAmount !=0)
        {  
            return true; 
        }
        else if (this.totalAmount == 0 && (currency == 'SGD' || currency == 'GBP'))
        {
            this.currency = currency;
            return true;
        }
        else 
        {
            return false;
        }
    }

    //Validate input data
    public validateInputData(data:any, res:Response):boolean
    {
        if(!data.currency)
        {
            res.status(422).json({
                'message':'Please provide a currency for this transaction'
            });
            return false;
        }
        if(!data.typeofTransaction || !(data.typeofTransaction == 'credit' || data.typeofTransaction == 'debit' ))
        {
            res.status(422).json({
                'message':'Please provide a type of transaction (credit or debit?)'
            });
            return false;
        }
        if(!data.transactionDescription)
        {
            res.status(422).json({
                'message':'Please provide a valid transaction description'
            });
            return false;
        }
        if(!data.amount && !Number.isNaN(data.amount))
        {
            res.status(422).json({
                'message':'Please provide a valid amount for this transaction, should be positive number (eg: 50.20)'
            });
            return false;
        }
        if(!data.accountNumber || !(data.accountNumber.toString().length == 13 && !Number.isNaN(data.accountNumber)))
        {
            res.status(422).json({
                'message':'Please provide a valid account number for this transaction, it should be 13 digits long and should be numeric.'
            });
            return false;
        }
        else
        {
            return true;
        }
    }
    //Add new transaction to record
    public addNewTransaction = async (req:Request, res:Response):Promise<void> =>
    {
        
        
        if(this.validateInputData(req.body,res))
        {
            await this.getDataForAccount(req.body.accountNumber);
            if(!this.checkCurrency(req.body.currency))
            {
                res.status(422).json({
                    'message': 'Please enter a valid currency that is being used by this account, we support only SGD or GBP for now.'
                });
            }
            else
            {
                if(this.calculateTotalAmount(req.body.typeofTransaction,parseFloat(req.body.amount),res)){
                    let newTransaction = new Transaction({
                        accountNumber:req.body.accountNumber,
                        currency:this.currency,
                        transactionDescription:req.body.transactionDescription,
                        typeofTransaction:req.body.typeofTransaction,
                        amount:req.body.amount,
                        totalBalance:this.totalAmount
                    });
                    newTransaction.save((err, transaction) => {
                        if(err)
                        {
                            res.send(err);
                        }
                        res.status(201).json(transaction);
                    });
                }
            }
        }
    }
    //Get Transactions of an account number
    public getTransactions (req:Request, res:Response): void{
        if(!req.query.accountNumber || !(req.query.accountNumber.toString().length == 13 && !Number.isNaN(req.query.accountNumber)))
        {
            res.status(422).json({
                'message':'Please provide a valid account number for which data needs to be fetched, it should be 13 digits long and should be numeric.'
            });
            
        }
        else{
            Transaction.find({'accountNumber':req.query.accountNumber}, "accountNumber transactionDescription typeofTransaction currency amount totalBalance created_date",{sort:{'created_date':'desc'}},(err, transactions) => {
                if(err){
                    res.status(200).send(err);
                }
                res.json(transactions);
            });
        }
    }

}