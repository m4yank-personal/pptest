import { config } from "dotenv";

let path;
switch (process.env.NODE_ENV) {
  case "test":
    path = `${__dirname}/../../.env.test`;
    break;
  case "production":
    path = `${__dirname}/../../.env`;
    break;
  case "development":
    path = `${__dirname}/../../.env.development`;
    break;
  default:
    path = `${__dirname}/../../.env.development`;
}
config({ path: path });
;