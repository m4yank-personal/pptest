import {Application,Request,Response} from 'express';
import {TransactionController} from '../controllers/transactionsController';

export class TransactionRoutes{

    public transactionController: TransactionController = new TransactionController();

    public routes(app: Application):void{
        app.route('/transactions')
        .get(this.transactionController.getTransactions)
        .post(this.transactionController.addNewTransaction);
    }

}