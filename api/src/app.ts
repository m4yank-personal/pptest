import express, {Application, Request, Response, NextFunction} from "express";
import * as bodyParser from 'body-parser';
import {TransactionRoutes} from "./routes/transactions";
import mongoose from "mongoose";

//Get the global configs for the project
import './config/config';

class App{
    public app: Application;
    public transactionRoutes: TransactionRoutes = new TransactionRoutes();
    public server: any | undefined;

    constructor(){
        this.app = express();
        this.config();
        this.transactionRoutes.routes(this.app);
        this.mongoSetup();
        this.initServer();
    }

    private config():void{
        this.app.use(bodyParser.json());
        this.app.use(bodyParser.urlencoded({ extended: false }));   
        this.app.use(function(req:Request, res:Response, next:NextFunction):void {
            res.header("Access-Control-Allow-Origin", "*");
            res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
            next();
          });       
    }

    private mongoSetup = ():void => {
        var mongoDBURI: string = String(process.env.MONGO_URI);
        mongoose.Promise = global.Promise;
        mongoose.connect(mongoDBURI, {useNewUrlParser: true, useUnifiedTopology: true}, function(error){
            if(!error)
            {
                console.log("Database is connected");
            }
        }); 
    }

    private initServer():void{
        this.server = this.app.listen(process.env.PORT, ()=>{console.log(`Server is running in ${process.env.NODE_ENV} mode on port ${process.env.PORT}`)});
    }
}
const Server:App = new App();
export default Server;