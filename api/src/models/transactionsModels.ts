import * as mongoose from 'mongoose';

const Schema = mongoose.Schema;

export const TransactionsSchema: mongoose.Schema = new Schema({

    accountNumber:{
        type: Schema.Types.Number,
        required:'Please enter account number'
    },
    currency:{
        type: Schema.Types.String,
        required:'A valid currency is required',
        enum: ['SGD', 'GBP']
    },
    transactionDescription:{
        type: Schema.Types.String,
        required: 'A valid description of transaction is required'
    },
    typeofTransaction:{
        type: Schema.Types.String,
        required: 'A valid transaction type is required',
        enum:['credit','debit']
    },
    amount:{
        type: Number,
        required: 'Amount with upto two decimal points is required',
    },
    totalBalance:{
        type: Number,
        min:0,
    },
    created_date:{
        type:Date,
        default:Date.now
    }
});

TransactionsSchema.path('amount').get(function(num: number): string{
    
    return (num/100).toFixed(2).toLocaleString();
});

TransactionsSchema.path('amount').set(function(num: number): number{
    return num*100;
});

TransactionsSchema.path('totalBalance').get(function(num: number): string{
    return (num/100).toFixed(2).toLocaleString();
});

TransactionsSchema.path('totalBalance').set(function(num: number): number{
    return num*100;
});

TransactionsSchema.set('toJSON', { getters: true });