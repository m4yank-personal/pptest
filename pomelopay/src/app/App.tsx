import * as React from 'react';

// import main containers
import TransactionContainer from '../transaction/TransactionContainer';

const App: React.SFC<{}> = () => <TransactionContainer />;

export default App;
