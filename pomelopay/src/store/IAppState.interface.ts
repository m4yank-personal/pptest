import ITransactionState from "../transaction/data/ITransactionState.interface";

export default interface IAppState {
  transactionState: ITransactionState;
}
