// Third-party dependencies
import{ applyMiddleware, combineReducers, createStore, Store } from 'redux';

// React Sagas
import createSagaMiddleware from 'redux-saga';

// Business domain imports
import IAppState from './IAppState.interface';
import TransactionReducer from '../transaction/reducers/TransactionReducer';
import { transactionSaga } from '../transaction/sagas/Transaction';

// Chrome Dev Tools
import { composeWithDevTools } from 'redux-devtools-extension';

// Saga Middleware
const sagaMiddleware = createSagaMiddleware();

// Create the root reducer
const rootReducer = combineReducers<IAppState>({
  transactionState: TransactionReducer,
});


// Create a configure store function of type `IAppState`
export default function configureStore(): Store<IAppState, any> {
  const store = createStore(
                  rootReducer,
                  undefined,
                  composeWithDevTools(applyMiddleware(sagaMiddleware))
                );

  sagaMiddleware.run(transactionSaga);

  return store;
}
