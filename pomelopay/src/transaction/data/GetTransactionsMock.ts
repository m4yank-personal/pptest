import ITransaction from './ITransaction.interface';
import GetTransactionMock from './GetTransactionMock';

const GetTransactionsMock: ITransaction[] = [GetTransactionMock];

export default GetTransactionsMock;
