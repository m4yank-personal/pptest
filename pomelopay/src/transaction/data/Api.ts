import axios from 'axios';

const baseUrl = 'http://localhost:3000';

export const getTransactionFromApi = (accountNumber:number): Promise<any> => {
  return axios.get(`${baseUrl}/transactions/?accountNumber=${accountNumber}`);
}