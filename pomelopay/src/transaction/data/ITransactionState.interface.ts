import ITransaction from './ITransaction.interface';

export default interface ITransactionState {
  readonly transaction?: ITransaction,
  readonly transactions: ITransaction[],
  readonly isFetching: Boolean,
}
