import ITransaction from './ITransaction.interface';

const GetTransactionMock: ITransaction = {
  id: '5e5b5c32f7b6933825499b4d',
  accountNumber: '1231231223123',
  currency: 'SGD',
  transactionDescription: 'Add 1000 Pounds',
  typeofTransaction: 'credit',
  totalBalance: 1000.00,
  amount:0,
  created_date:''
}

export default GetTransactionMock;