export default interface ITransaction {
    id: string;
    accountNumber: string;
    currency: string;
    created_date: string;
    amount:number,
    transactionDescription: string;
    typeofTransaction: string;
    totalBalance: number;
  }