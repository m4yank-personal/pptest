import * as React from 'react';
// import interface for transaction
import ITransaction from './data/ITransaction.interface';
import DataTable from 'react-data-table-component';
import Moment from 'react-moment';

interface IProps {
  transactions: ITransaction[];
}
  const columns = [
    {
      name: 'ID',
      selector: 'id',
      sortable: true,
    },
    {
      name: 'Date',
      selector: 'created_date',
      sortable: true,
      right: false,
      
      cell: (row: { created_date: string; }) => <span><Moment format="YYYY/MM/DD HH:mm:ss">{row.created_date}</Moment></span>
    },
    {
        name: 'Account Number',
        selector: 'accountNumber',
    },
    {
      name: 'Description',
      selector: 'transactionDescription',
      right:false,
    },
    {
        name: 'Credit/Debit',
        selector: 'typeofTransaction',
        sortable: true,
        right:false,
    },
    {
      name: 'Amount',
      selector: 'amount',
      sortable: true,
      right:true,
     
    },
    {
      name: 'Balance',
      selector: 'totalBalance',
      sortable: true,
      right:true,
    },
      
  ];
const TransactionList: React.FunctionComponent<IProps> = ({ transactions }) => (
  <ul className="list-group">
      <DataTable
        title="Transactions"
        columns={columns}
        data={transactions}
    />
  </ul>
);

export default TransactionList;
