import React from 'react';
import { connect } from 'react-redux';

import IAppState from '../store/IAppState.interface';
import ITransaction from './data/ITransaction.interface';

import {
  getTransactionsStartActionCreator
} from './actions/TransactionActionCreators';


import TransactionList from './TransactionList';
import Loader from './Loader';


interface IProps {
  getTransactions: Function,
  transaction: any,
  transactions: ITransaction[],
  isFetching: Boolean
}

// Note: This is mainly done to enable testing
export const TransactionContainer: React.FunctionComponent<IProps> = ({
  getTransactions,
  transactions,
  isFetching
}) => {
  // Workaround for Enyzme testing of useEffect, allows stubbing
  // See: https://blog.carbonfive.com/2019/08/05/shallow-testing-hooks-with-enzyme/
  React.useEffect(() => {
    getTransactions();
  }, [getTransactions]);

  return (
    <div className="transactions-container">
      { isFetching
        ?  <Loader/>
        : (
          <div className="row">
              <TransactionList transactions={transactions} />
          </div>
        )
      }
    </div>
  );
}

// Make data available on props
const mapStateToProps = (store: IAppState) => {
  return {
    transaction: store.transactionState.transaction,
    transactions: store.transactionState.transactions,
    isFetching: store.transactionState.isFetching,
  };
};

// Make functions available on props
const mapDispatchToProps = (dispatch: any) => {
  return {
    getTransactions: () => dispatch(getTransactionsStartActionCreator())
  }
}

// Connect the app aware container to the store and reducers
export default connect(mapStateToProps, mapDispatchToProps)(TransactionContainer);
