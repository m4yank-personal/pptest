// Business domain imports
import TransactionActionTypes from './TransactionActionTypes.enum';
import {  IGetTransactionsStartAction,  IGetTransactionsSuccessAction,  IGetTransactionsFailureAction } from './IGetTransactionsActions.interface';
import ITransaction from '../data/ITransaction.interface';


export const getTransactionsStartActionCreator = (): IGetTransactionsStartAction => {
 
  return {
    type: TransactionActionTypes.GET_TRANSACTIONS_START,
    isFetching: true,
  };
}

export const getTransactionsSuccessActionCreator = (transactions: ITransaction[]): IGetTransactionsSuccessAction => {
  return {
    type: TransactionActionTypes.GET_TRANSACTIONS_SUCCESS,
    transactions,
    isFetching: false,
  };
}

export const getTransactionsFailureActionCreator = (): IGetTransactionsFailureAction => {
  return {
    type: TransactionActionTypes.GET_TRANSACTIONS_FAILURE,
    isFetching: false,
  };
}
