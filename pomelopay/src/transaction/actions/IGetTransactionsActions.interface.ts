import ITransaction from '../data/ITransaction.interface';
import TransactionActionTypes from './TransactionActionTypes.enum';

export interface IGetTransactionsStartAction {
  type: TransactionActionTypes.GET_TRANSACTIONS_START,
  isFetching: true,
}
export interface IGetTransactionsSuccessAction {
  type: TransactionActionTypes.GET_TRANSACTIONS_SUCCESS,
  transactions: ITransaction[],
  isFetching: false,
}
export interface IGetTransactionsFailureAction {
  type: TransactionActionTypes.GET_TRANSACTIONS_FAILURE,
  isFetching: false,
}
