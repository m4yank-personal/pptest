import {
    IGetTransactionsStartAction,
    IGetTransactionsSuccessAction,
    IGetTransactionsFailureAction
  } from './IGetTransactionsActions.interface';
  
  // Combine the action types with a union (we assume there are more)
  type TransactionActions =
    IGetTransactionsStartAction
    | IGetTransactionsSuccessAction
    | IGetTransactionsFailureAction;
  
  export default TransactionActions;
  