import { call, put, takeEvery, all } from 'redux-saga/effects';
import TransactionActionTypes from '../actions/TransactionActionTypes.enum';

import {
    getTransactionFromApi
} from '../data/Api';

import {
  getTransactionsSuccessActionCreator,
  getTransactionsFailureActionCreator
} from '../actions/TransactionActionCreators';

export function* getTransactionsSaga() : any {
  try {
    const accountNumber:number = 1231231223123;
    const response = yield call(getTransactionFromApi,accountNumber);
    const transactions = response.data;
    yield put(getTransactionsSuccessActionCreator(transactions))
  } catch(e) {
    yield put(getTransactionsFailureActionCreator());
  }
}


export function* transactionSaga() {
  yield all([
    takeEvery(TransactionActionTypes.GET_TRANSACTIONS_START, getTransactionsSaga)
  ]);
}
