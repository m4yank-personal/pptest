// Import Reducer type
import { Reducer } from 'redux';

// Busines domain imports
import TransactionAction from '../actions/TransactionActions.type';
import TransactionActionTypes from '../actions/TransactionActionTypes.enum';
import ITransactionState from '../data/ITransactionState.interface';

// Business logic
const initialTransactionState: ITransactionState = {
  transaction: undefined,
  transactions: [],
  isFetching: false,
};

const TransactionReducer: Reducer<ITransactionState, TransactionAction> = (
  state = initialTransactionState,
  action
) => {
  switch (action.type) {
    case TransactionActionTypes.GET_TRANSACTIONS_START: {
      return {
        ...state,
        isFetching: action.isFetching,
      };
    }
    case TransactionActionTypes.GET_TRANSACTIONS_SUCCESS: {
      return {
        ...state,
        transactions: action.transactions,
        isFetching: action.isFetching,
      };
    }
    case TransactionActionTypes.GET_TRANSACTIONS_FAILURE: {
      return {
        ...state,
        isFetching: action.isFetching,
      };
    }
    default:
      return state;
  }
};

export default TransactionReducer;
